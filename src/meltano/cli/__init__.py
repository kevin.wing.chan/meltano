from .main import cli
from . import elt, schema, discovery, initialize, add, install, invoke


def main():
    cli()
